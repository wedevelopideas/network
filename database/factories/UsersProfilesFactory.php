<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use Faker\Generator as Faker;
use App\network\Users\Models\UsersProfiles;

$factory->define(UsersProfiles::class, function (Faker $faker) {
    return [
        'user_id' => function () {
            return factory(\App\network\Users\Models\Users::class)->create()->id;
        },
        'birthday' => $faker->date('Y-m-d'),
        'gender' => $faker->randomElement(['female', 'male']),
        'significant_other' => function () {
            return factory(\App\network\Users\Models\Users::class)->create()->id;
        },
        'anniversary' => $faker->date('Y-m-d'),
        'bio' => $faker->text,
    ];
});
