<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use Faker\Generator as Faker;
use App\network\Users\Models\UsersFriends;

$factory->define(UsersFriends::class, function (Faker $faker) {
    return [
        'friend_id' => function () {
            return factory(\App\network\Users\Models\Users::class)->create()->id;
        },
        'user_id' => function () {
            return factory(\App\network\Users\Models\Users::class)->create()->id;
        },
        'status' => $faker->randomNumber(),
    ];
});
