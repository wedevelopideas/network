<?php

namespace App\network\Users\Repositories;

use App\network\Users\Models\UsersProfiles;

class UsersProfilesRepository
{
    /**
     * @var UsersProfiles
     */
    private $usersProfiles;

    /**
     * UsersProfilesRepository constructor.
     * @param UsersProfiles $usersProfiles
     */
    public function __construct(UsersProfiles $usersProfiles)
    {
        $this->usersProfiles = $usersProfiles;
    }

    /**
     * Get user data by its id.
     *
     * @param int $user_id
     * @return mixed
     */
    public function getDataByUserID(int $user_id)
    {
        return $this->usersProfiles
            ->where('user_id', $user_id)
            ->first();
    }
}
