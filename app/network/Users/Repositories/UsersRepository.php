<?php

namespace App\network\Users\Repositories;

use App\network\Users\Models\Users;

class UsersRepository
{
    /**
     * @var Users
     */
    private $users;

    /**
     * UsersRepository constructor.
     * @param Users $users
     */
    public function __construct(Users $users)
    {
        $this->users = $users;
    }

    /**
     * Get all users.
     *
     * @return mixed
     */
    public function getAll()
    {
        return $this->users
            ->orderBy('last_name', 'ASC')
            ->orderBy('first_name', 'ASC')
            ->get();
    }

    /**
     * Get data from user by id.
     *
     * @param int $id
     * @return mixed
     */
    public function getUserByID(int $id)
    {
        return $this->users
            ->where('id', $id)
            ->first();
    }

    /**
     * Get data from user by username.
     *
     * @param string $user_name
     * @return mixed
     */
    public function getUserByUserName(string $user_name)
    {
        return $this->users
            ->where('user_name', $user_name)
            ->first();
    }
}
