<?php

namespace App\network\Users\Repositories;

use App\network\Users\Models\UsersFriends;

class UsersFriendsRepository
{
    /**
     * @var UsersFriends
     */
    private $usersFriends;

    /**
     * UsersFriendsRepository constructor.
     * @param UsersFriends $usersFriends
     */
    public function __construct(UsersFriends $usersFriends)
    {
        $this->usersFriends = $usersFriends;
    }

    /**
     * Get all friends of the user.
     *
     * @param int $user_id
     * @return mixed
     */
    public function getFriends(int $user_id)
    {
        return $this->usersFriends
            ->join('users', 'users.id', '=', 'users_friends.friend_id')
            ->where('users_friends.user_id', $user_id)
            ->orderBy('users.last_name', 'ASC')
            ->orderBy('users.first_name', 'ASC')
            ->get();
    }
}
