<?php

namespace App\network\Users\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class UsersProfiles.
 *
 * @property int $id
 * @property int $user_id
 * @property \Illuminate\Support\Carbon|null $birthday
 * @property string $gender
 * @property int $significant_other
 * @property \Illuminate\Support\Carbon|null $anniversary
 * @property string $bio
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 */
class UsersProfiles extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id',
        'birthday',
        'gender',
        'significant_other',
        'anniversary',
        'bio',
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        'birthday',
        'anniversary',
    ];

    /**
     * User relationship for partner.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function partner()
    {
        return $this->belongsTo(Users::class, 'significant_other');
    }
}
