<?php

namespace App\network\Users\Models;

use Illuminate\Foundation\Auth\User as Authenticatable;

/**
 * Class Users.
 *
 * @property int $id
 * @property string $email
 * @property string $password
 * @property string $first_name
 * @property string $middle_name
 * @property string $last_name
 * @property string $display_name
 * @property string $user_name
 * @property string $profile_image
 * @property bool $is_admin
 * @property bool $is_blocked
 * @property bool $is_verified
 * @property string|null $remember_token
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 */
class Users extends Authenticatable
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'email',
        'password',
        'first_name',
        'middle_name',
        'last_name',
        'display_name',
        'user_name',
        'profile_image',
        'is_admin',
        'is_blocked',
        'is_verified',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'is_admin' => 'boolean',
        'is_blocked' => 'boolean',
        'is_verified' => 'boolean',
    ];

    /**
     * Shows profile image of the user.
     *
     * @return string
     */
    public function getImage()
    {
        return 'http://media.network.granath/users/'.$this->profile_image;
    }
}
