<?php

namespace App\network\Users\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class UsersFriends.
 *
 * @property int $id
 * @property int $friend_id
 * @property int $user_id
 * @property int $status
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 */
class UsersFriends extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'friend_id',
        'user_id',
        'status',
    ];

    /**
     * Users relationship.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(Users::class, 'friend_id');
    }
}
