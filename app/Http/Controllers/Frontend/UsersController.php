<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\network\Users\Repositories\UsersRepository;
use App\network\Users\Repositories\UsersFriendsRepository;
use App\network\Users\Repositories\UsersProfilesRepository;

class UsersController extends Controller
{
    /**
     * @var UsersRepository
     */
    private $usersRepository;

    /**
     * @var UsersFriendsRepository
     */
    private $usersFriendsRepository;

    /**
     * @var UsersProfilesRepository
     */
    private $usersProfilesRepository;

    /**
     * UsersController constructor.
     * @param UsersRepository $usersRepository
     * @param UsersFriendsRepository $usersFriendsRepository
     * @param UsersProfilesRepository $usersProfilesRepository
     */
    public function __construct(UsersRepository $usersRepository,
                                UsersFriendsRepository $usersFriendsRepository,
                                UsersProfilesRepository $usersProfilesRepository)
    {
        $this->usersRepository = $usersRepository;
        $this->usersFriendsRepository = $usersFriendsRepository;
        $this->usersProfilesRepository = $usersProfilesRepository;
    }

    /**
     * Get the about page of the user.
     *
     * @param string $user_name
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function about(string $user_name)
    {
        $user = $this->usersRepository->getUserByUserName($user_name);

        // User is unavailable
        if (! $user) {
            abort(404);
        }

        $about = $this->usersProfilesRepository->getDataByUserID($user->id);

        return view('users.about')
            ->with('about', $about)
            ->with('user', $user);
    }

    /**
     * Get the friends of the user.
     *
     * @param string $user_name
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function friends(string $user_name)
    {
        $user = $this->usersRepository->getUserByUserName($user_name);

        // User is unavailable
        if (! $user) {
            abort(404);
        }

        $friends = $this->usersFriendsRepository->getFriends($user->id);

        return view('users.friends')
            ->with('friends', $friends)
            ->with('user', $user);
    }

    /**
     * Get the profile of the user.
     *
     * @param string $user_name
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function profile(string $user_name)
    {
        $user = $this->usersRepository->getUserByUserName($user_name);

        // User is unavailable
        if (! $user) {
            abort(404);
        }

        return view('users.profile')
            ->with('user', $user);
    }
}
