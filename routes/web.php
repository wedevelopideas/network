<?php

use Illuminate\Support\Facades\Route;

Route::group(['namespace' => 'Backend', 'prefix' => 'backend', 'middleware' => 'auth'], function () {
    Route::get('', ['uses' => 'HomeController@index', 'as' => 'backend']);

    Route::group(['prefix' => 'users'], function () {
        Route::get('', ['uses' => 'UsersController@index', 'as' => 'backend.users']);
    });
});

Route::group(['namespace' => 'Frontend'], function () {
    Route::get('', ['uses' => 'HomeController@index', 'as' => 'index']);

    Route::group(['namespace' => 'Auth'], function () {
        Route::get('login', ['uses' => 'LoginController@showForm', 'as' => 'login']);
        Route::post('login', ['uses' => 'LoginController@handleRequest']);
        Route::get('logout', ['uses' => 'LoginController@logout', 'as' => 'logout']);
    });

    Route::group(['middleware' => 'auth'], function () {
        Route::get('home', ['uses' => 'HomeController@home', 'as' => 'home']);

        Route::get('{user_name}', ['uses' => 'UsersController@profile', 'as' => 'user']);
        Route::get('{user_name}/about', ['uses' => 'UsersController@about', 'as' => 'user.about']);
        Route::get('{user_name}/friends', ['uses' => 'UsersController@friends', 'as' => 'user.friends']);
    });
});
