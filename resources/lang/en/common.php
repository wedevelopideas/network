<?php

return [
    'about' => 'About',
    'created_at' => 'Created at',
    'email' => 'Email',
    'friends' => 'Friends',
    'home' => 'Home',
    'login' => 'Login',
    'password' => 'Password',
    'user' => 'User',
    'users' => 'Users',
];
