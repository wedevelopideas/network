<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>@yield('title')</title>
    <link rel="stylesheet" href="{{ asset('assets/semantic.min.css') }}">
    <style type="text/css">
        body {
            background-color: #FFFFFF;
        }
        .main.container {
            margin-top: 7em;
        }
        @yield('styles')
    </style>
</head>
<body>
    <div class="ui fixed inverted menu">
        <div class="ui fluid container">
            <a href="{{ route('home') }}" class="header item">
                Network
            </a>
            <a href="{{ route('backend.users') }}" class="icon item">
                <i class="users icon"></i>
            </a>
            <div class="right menu">
                <div class="item">
                    <img src="{{ auth()->user()->getImage() }}" alt="{{ auth()->user()->display_name }}" class="ui avatar image">
                    <span>
                        {{ auth()->user()->first_name }}
                    </span>
                </div>
                <a href="{{ route('logout') }}" class="icon item">
                    <i class="sign out icon"></i>
                </a>
            </div>
        </div>
    </div>
@yield('content')
<script src="{{ asset('assets/jquery.min.js') }}"></script>
<script src="{{ asset('assets/semantic.min.js') }}"></script>
@yield('scripts')
</body>
</html>