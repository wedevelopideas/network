@extends('layouts.frontend')
@section('title', 'Network | '.$user->display_name)
@section('content')
    <div class="ui main container">
        <div class="ui stackable grid">
            <div class="row">
                <div class="column">
                    <div class="ui small image">
                        <div class="ui bottom attached label">
                            {{ $user->display_name }}
                        </div>
                        <img src="{{ $user->getImage() }}" alt="{{ $user->display_name }}">
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="four wide column">
                    @include('users._partials.sidebar')
                </div>
                <div class="twelve wide column"></div>
            </div>
        </div>
    </div>
@endsection