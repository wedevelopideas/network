@extends('layouts.frontend')
@section('title', 'Network | '.$user->display_name)
@section('content')
    <div class="ui main container">
        <div class="ui stackable grid">
            <div class="row">
                <div class="column">
                    <div class="ui small image">
                        <div class="ui bottom attached label">
                            {{ $user->display_name }}
                        </div>
                        <img src="{{ $user->getImage() }}" alt="{{ $user->display_name }}">
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="four wide column">
                    @include('users._partials.sidebar')
                </div>
                <div class="twelve wide column">
                    @if (isset($about))
                        <div class="ui relaxed divided list">
                            @if (!is_null($about->birthday))
                                <div class="item">
                                    <i class="birthday middle aligned icon"></i>
                                    <div class="content">
                                        {{ $about->birthday->format('d.m.Y') }}
                                    </div>
                                </div>
                            @endif
                            @if ($about->gender)
                                <div class="item">
                                    <i class="{{ $about->gender }} middle aligned icon"></i>
                                    <div class="content">
                                        {{ $about->gender }}
                                    </div>
                                </div>
                            @endif
                        </div>
                        @if ($about->significant_other)
                            <div class="ui relaxed divided list">
                                <div class="item">
                                    <img src="{{ $about->partner->getImage() }}" alt="{{ $about->partner->display_name }}" class="ui avatar image">
                                    <div class="content">
                                        <a href="{{ route('user', ['user_name' => $about->partner->user_name]) }}" class="header">
                                            {{ $about->partner->display_name }}
                                        </a>
                                        <div class="description">
                                            {{ $about->anniversary->format('d.m.Y') }}
                                            ({{ $about->anniversary->diffForHumans() }})
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endif
                    @endif
                </div>
            </div>
        </div>
    </div>
@endsection