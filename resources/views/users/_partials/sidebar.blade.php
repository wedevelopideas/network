<div class="ui secondary vertical pointing menu">
    <a href="{{ route('user', ['user_name' => $user->user_name]) }}" class="item">
        {{ trans('common.home') }}
    </a>
    <a href="{{ route('user.about', ['user_name' => $user->user_name]) }}" class="item">
        {{ trans('common.about') }}
    </a>
    <a href="{{ route('user.friends', ['user_name' => $user->user_name]) }}" class="item">
        {{ trans('common.friends') }}
    </a>
</div>