@extends('layouts.blank')
@section('title', 'Network')
@section('content')
    <div class="ui middle aligned center aligned grid">
        <div class="column">
            <h2 class="ui blue header">
                Network
            </h2>
            <form action="{{ route('login') }}" method="post" class="ui large form">
                @csrf

                <div class="ui stacked segment">
                    <div class="field{{ $errors->has('email') ? ' error' : '' }}">
                        <div class="ui left icon input">
                            <i class="user icon"></i>
                            <input type="text" name="email" autocomplete="off" placeholder="{{ trans('common.email') }}">
                        </div>
                    </div>
                    <div class="field{{ $errors->has('password') ? ' error' : '' }}">
                        <div class="ui left icon input">
                            <i class="lock icon"></i>
                            <input type="password" name="password" autocomplete="off" placeholder="{{ trans('common.password') }}">
                        </div>
                    </div>
                    <button type="submit" class="ui fluid large blue submit button">{{ trans('common.login') }}</button>
                </div>
            </form>
        </div>
    </div>
@endsection
@section('styles')
    <style type="text/css">

        body {
            background-color: #DADADA;
        }
        body > .grid {
            height: 100%;
        }
        .column {
            max-width: 450px;
        }

    </style>
@endsection