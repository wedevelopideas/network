<?php

namespace Tests\Feature;

use Tests\TestCase;

class UsersTest extends TestCase
{
    /** @test */
    public function test_shows_user_about()
    {
        $response = $this->actingAs($this->user)->get(route('user.about', ['user_name' => $this->user->user_name]));
        $response->assertSuccessful();
    }

    /** @test */
    public function test_shows_failed_user_about()
    {
        $response = $this->actingAs($this->user)->get(route('user.about', ['user_name' => 'john-doe']));
        $response->assertNotFound();
    }

    /** @test */
    public function test_shows_user_friends()
    {
        $response = $this->actingAs($this->user)->get(route('user.friends', ['user_name' => $this->user->user_name]));
        $response->assertSuccessful();
    }

    /** @test */
    public function test_shows_failed_user_friends()
    {
        $response = $this->actingAs($this->user)->get(route('user.friends', ['user_name' => 'john-doe']));
        $response->assertNotFound();
    }

    /** @test */
    public function test_shows_user_profile()
    {
        $response = $this->actingAs($this->user)->get(route('user', ['user_name' => $this->user->user_name]));
        $response->assertSuccessful();
    }

    /** @test */
    public function test_shows_failed_user_profile()
    {
        $response = $this->actingAs($this->user)->get(route('user', ['user_name' => 'john-doe']));
        $response->assertNotFound();
    }
}
