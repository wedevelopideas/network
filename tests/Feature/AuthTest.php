<?php

namespace Tests\Feature;

use Tests\TestCase;

class AuthTest extends TestCase
{
    /** @test */
    public function test_shows_login_form()
    {
        $response = $this->get(route('login'));
        $response->assertSuccessful();
    }

    /** @test */
    public function test_successful_login()
    {
        $response = $this->post(route('login'), ['email' => $this->user->email, 'password' => 'password']);
        $response->assertStatus(302);
        $response->assertRedirect(route('home'));
    }

    /** @test */
    public function test_failed_login()
    {
        $response = $this->post(route('login'), ['email' => $this->user->email, 'password' => 'xxxxyyyy']);
        $response->assertStatus(302);
        $response->assertRedirect(route('login'));
    }

    /** @test */
    public function test_successful_logout()
    {
        $response = $this->actingAs($this->user)->get(route('logout'));
        $response->assertStatus(302);
        $response->assertRedirect(route('login'));
    }

    /** @test */
    public function test_access_to_dashboard()
    {
        $response = $this->get(route('home'));
        $response->assertRedirect('login');
    }

    /** @test */
    public function test_redirect_if_authenticated()
    {
        $response = $this->actingAs($this->user)->get(route('login'));
        $response->assertRedirect(route('home'));
    }

    /** @test */
    public function test_not_redirect_when_using_an_ajax_request()
    {
        $response = $this->json('get', route('home'));
        $response->assertStatus(401);
    }
}
